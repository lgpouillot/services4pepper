#!/usr/bin/env python
# *-* coding: utf-8 *-* 

import qi
import sys
import os

import logging

class BGService:
    services_connected = None
    connected_signals = []

    def __init__(self, application):
        # Getting a session that will be reused everywhere
        self.application = application
        self.session = application.session
        self.service_name = self.__class__.__name__

        # Getting a logger. Logs will be in /var/log/naoqi/servicemanager/{application id}.{service name}
        self.logger = qi.Logger(self.service_name)
        #self.logger = self.logging

        # Do some initializations before the service is registered to NAOqi
        self.logger.info("Initializing...")
        self.lang = "English"
        self.connect_services()
        self.create_signals()
        self.logger.info("Initialized!")

    @qi.nobind
    def start_service(self):
        self.logger.info("Starting service...")
        # start what you need
        self.logger.info("Started!")

    def stop_service(self):
        # probably useless, unless one method needs to stop the service from inside.
        # external naoqi scripts should use ALServiceManager.stopService if they need to stop it.
        self.logger.info("Stopping service...")
        self.application.stop()
        self.logger.info("Stopped!")

    @qi.nobind
    def connect_services(self):
        # connect all services required by your module
        # done in async way over 30s,
        # so it works even if other services are not yet ready when you start your module
        # this is required when the service is autorun as it may start before other modules...
        self.logger.info('Connecting services...')
        self.services_connected = qi.Promise()
        services_connected_fut = self.services_connected.future()

        def get_services():
            try:
                self.memory = self.session.service('ALMemory')
                self.bcr    = self.session.service('ALBarcodeReader')
                self.audiop = self.session.service('ALAudioPlayer')

                #####################################################
                # ... connect to proper services ...
                #####################################################

                self.logger.info('All services are now connected')
                self.services_connected.setValue(True)
            except RuntimeError as e:
                self.logger.warning('Still missing some service:\n {}'.format(e))

        get_services_task = qi.PeriodicTask()
        get_services_task.setCallback(get_services)
        get_services_task.setUsPeriod(int(2*1000000))  # check every 2s
        get_services_task.start(True)
        try:
            services_connected_fut.value(30*1000)  # timeout = 30s
            get_services_task.stop()
        except RuntimeError:
            get_services_task.stop()
            self.logger.error('Failed to reach all services after 30 seconds')
            raise RuntimeError

    @qi.nobind
    def create_signals(self):
        self.logger.info("Creating Training events...")

        self.add_memory_subscriber(self.service_name+"/Exit", self.on_exit)

        ###################################################################
        # ... add more memory subscribers ...
        ###################################################################

        self.logger.info("Events created!")

    @qi.nobind
    def on_exit(self, value):
        self.logger.info("On exit is called")
        self.application.stop()

    ############### Utility functions ##########################
    @qi.nobind
    def add_memory_subscriber(self, event, callback):
        # add memory subscriber utility function
        self.logger.info("Subscribing to {}".format(event))
        try:
            sub = self.memory.subscriber(event)
            con = sub.signal.connect(callback)
            self.connected_signals.append([sub, con])
        except Exception, e:
            self.logger.info("Error while subscribing: {}".format(e))

    @qi.nobind
    def remove_memory_subscribers(self):
        # remove memory subscribers utility function
        self.logger.info("unsubscribing to all signals...")
        for sub, con in self.connected_signals:
            try:
                sub.signal.disconnect(con)
            except Exception, e:
                self.logger.info("Error while unsubscribing: {}".format(e))

    ############################################################

    #################### Robot Volume #########################
    #
    #
    # Exercice 1 :
    #
    # TODO : create functions to increase / decrease sound volume : volume_up and volume_down
    # TODO : 1/ create two events to trigger these functions: TrainingService/volume_up and TrainingService/volume_down )
    # TODO : 2/ also expose these two functions in order to have them accessible in the API    I
    # TODO : test them with qicli (qicli call ALMemory.raiseEvent / qicli call TrainingService.volumeUp ... )
    # TODO : combine the use of two tactile sensors to avoid unwilled Head sensor volume change

    # ...your code here...

    #
    #
    ############################################################


    #################### Robot Movements ######################
    #
    # Exercice 2 
    #
    # TODO : expose these functions
    # TODO : complete the code to control Robot Movements
    #

    # ...your code here...

    #
    #
    #########################################################################

    
    #################### Subscribe Camera & Take Picture ######################
    #
    # Exercice 3 
    #
    # TODO : expose these functions
    # TODO : complete the code to subscribe camera in a choosen resolution and colorspace
    # TODO : complete the code to take a picture
    #

    # ...your code here...

    #
    #
    #########################################################################    
    
    
    #################### Request Weather for a webservice ######################
    #
    # Exercice 4 
    #
    # TODO : chose a weather webservice
    # TODO : import or embed proper libs
    # TODO : create callback and events to be used by other apps (ex: BGService/takePicture or BGService/rawData ... )
    #

    # ...your code here...

    #
    #
    #########################################################################    
    
    
    #################### Functions to send the data on a remote server (webservice, socket, ...) ######################
    #
    # Exercice 5 
    #
    # TODO : import or embed proper libs
    # TODO : create callback and events to be used by other apps

    # ...your code here...

    #
    #
    ######################################################################### 
    

    ####################################################################################
    # Section for QR Code detection
    ####################################################################################
    #
    # Exercice 6 (not much to do left...)
    # 
    def activate_qrcode_detection(self):
        """
        Activates qr code detection by subscribing to event BarcodeReader/BarcodeDetected and to ALBarcodeReader
        """
    
        # 1/ Connect the event to the callback function
        self.add_memory_subscriber("BarcodeReader/BarcodeDetected", self.on_bcr_detected)
        
        # 2/ We need to subscribe to ALBarCodeReader to get the extractor working
        self.bcr.subscribe(self.service_name) # we subscribe to ALBarCodeReader as it is an extractor
        self.bcr_detected = False

        self.logger.info("QRCode detection activated")

    def deactivate_qrcode_detection(self):
        """
        Deactivates qr code detection
        """
        try:
            self.bcr.unsubscribe( self.service_name)
        except:
            self.logger.warning("QRCode detection already deactivated")
            pass
            
        self.logger.info("QRCode detection deactivated")
      
        # Just for vocal feedback
        #self.tts.say("q r code detection deactivated")

    def on_bcr_detected(self, value):
        """
        Callback for event BarcodeReader/BarcodeDetected.
        """
        if value == []:  # empty value when the barcode disappears
            self.bcr_detected = False
        elif not self.bcr_detected:  # only speak the first time a face appears
            self.bcr_detected = True
                        
            # play a sound on qrcode detection
            self.audiop.playFile("/opt/aldebaran/share/naoqi/wav/bip_power_off.wav", _async=True)
            
            content = value[0][0]
            self.logger.info("QRCode content is: " + str(content))
            self.logger.info("Raising QRCode content")

            self.memory.raiseEvent(self.service_name+"/qrcode_content", content)
            self.memory.raiseEvent(self.service_name+"/htmlContent", content)
    #
    #
    #####################################################################################


    def cleanup(self):
        # called when your module is stopped
        self.logger.info("Cleaning...")
        # don't forget to finish cleanly
        self.deactivate_qrcode_detection() 
        self.remove_memory_subscribers()
        self.logger.info("End!")

if __name__ == "__main__":
    # with this you can run the script for tests on remote robots
    # run : python my_super_service.py --qi-url 123.123.123.123
    app = qi.Application(sys.argv)
    app.start()
    service_instance = BGService(app)
    service_id = app.session.registerService(service_instance.service_name, service_instance)
    service_instance.start_service()
    app.run()
    service_instance.cleanup()
    app.session.unregisterService(service_id)
