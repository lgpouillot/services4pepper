<?xml version="1.0" encoding="UTF-8" ?>
<Package name="FG_AppLauncher" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="." xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="training_service" src="training_service.py" />
        <File name="index" src="html/index.html" />
        <File name="main" src="html/main.js" />
        <File name="bootstrap-theme" src="html/bootstrap/css/bootstrap-theme.css" />
        <File name="bootstrap-theme.min" src="html/bootstrap/css/bootstrap-theme.min.css" />
        <File name="bootstrap" src="html/bootstrap/css/bootstrap.css" />
        <File name="bootstrap.min" src="html/bootstrap/css/bootstrap.min.css" />
        <File name="glyphicons-halflings-regular" src="html/bootstrap/fonts/glyphicons-halflings-regular.eot" />
        <File name="glyphicons-halflings-regular" src="html/bootstrap/fonts/glyphicons-halflings-regular.svg" />
        <File name="glyphicons-halflings-regular" src="html/bootstrap/fonts/glyphicons-halflings-regular.ttf" />
        <File name="glyphicons-halflings-regular" src="html/bootstrap/fonts/glyphicons-halflings-regular.woff" />
        <File name="jquery-3.3.1" src="html/bootstrap/jquery/jquery-3.3.1.js" />
        <File name="bootstrap" src="html/bootstrap/js/bootstrap.js" />
        <File name="bootstrap.min" src="html/bootstrap/js/bootstrap.min.js" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
