#!/usr/bin/env python
# *-* coding: utf-8 *-* 

import qi
import sys
import os

import logging

class FGAppLauncher:
    services_connected = None
    connected_signals = []

    def __init__(self, application):
        # Getting a session that will be reused everywhere
        self.application = application
        self.session = application.session
        self.service_name = self.__class__.__name__

        # Getting a logger. Logs will be in /var/log/naoqi/servicemanager/{application id}.{service name}
        self.logger = qi.Logger(self.service_name)
        #self.logger = self.logging

        # Do some initializations before the service is registered to NAOqi
        self.logger.info("Initializing...")
        self.lang = "English"
        self.connect_services()
        self.create_signals()
        self.logger.info("Initialized!")

    @qi.nobind
    def start_service(self):
        self.logger.info("Starting service...")
        # start what you need
        self.show_tablet()
        self.logger.info("Started!")

    def stop_service(self):
        # probably useless, unless one method needs to stop the service from inside.
        # external naoqi scripts should use ALServiceManager.stopService if they need to stop it.
        self.logger.info("Stopping service...")
        self.application.stop()
        self.logger.info("Stopped!")

    @qi.nobind
    def connect_services(self):
        # connect all services required by your module
        # done in async way over 30s,
        # so it works even if other services are not yet ready when you start your module
        # this is required when the service is autorun as it may start before other modules...
        self.logger.info('Connecting services...')
        self.services_connected = qi.Promise()
        services_connected_fut = self.services_connected.future()

        def get_services():
            try:
                self.memory = self.session.service('ALMemory')
                
                #####################################################
                # ... connect to proper services ...
                #####################################################

                self.logger.info('All services are now connected')
                self.services_connected.setValue(True)
            except RuntimeError as e:
                self.logger.warning('Still missing some service:\n {}'.format(e))

        get_services_task = qi.PeriodicTask()
        get_services_task.setCallback(get_services)
        get_services_task.setUsPeriod(int(2*1000000))  # check every 2s
        get_services_task.start(True)
        try:
            services_connected_fut.value(30*1000)  # timeout = 30s
            get_services_task.stop()
        except RuntimeError:
            get_services_task.stop()
            self.logger.error('Failed to reach all services after 30 seconds')
            raise RuntimeError

    @qi.nobind
    def create_signals(self):
        self.logger.info("Creating Training events...")

        self.add_memory_subscriber(self.service_name+"/Exit", self.on_exit)

        ###################################################################
        # ... add more memory subscribers ...
        ###################################################################

        self.logger.info("Events created!")

    @qi.nobind
    def on_exit(self, value):
        self.logger.info("On exit is called")
        self.application.stop()

    ############### Utility functions ##########################
    @qi.nobind
    def add_memory_subscriber(self, event, callback):
        # add memory subscriber utility function
        self.logger.info("Subscribing to {}".format(event))
        try:
            sub = self.memory.subscriber(event)
            con = sub.signal.connect(callback)
            self.connected_signals.append([sub, con])
        except Exception, e:
            self.logger.info("Error while subscribing: {}".format(e))

    @qi.nobind
    def remove_memory_subscribers(self):
        # remove memory subscribers utility function
        self.logger.info("unsubscribing to all signals...")
        for sub, con in self.connected_signals:
            try:
                sub.signal.disconnect(con)
            except Exception, e:
                self.logger.info("Error while unsubscribing: {}".format(e))

    def show_tablet(self):
        # how to load and display the webpage on the tablet
        dir_path = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__))))
        folder = os.path.basename(dir_path)
        self.logger.info("Loading tablet page for app: {}".format(folder))
        try:
            ts = self.session.service("ALTabletService")
            ts.loadApplication(folder)
            ts.showWebview()
        except Exception, e:
            self.logger.info("Error while loading tablet: {}".format(e))

    def hide_tablet(self):
        self.logger.info("Hiding tablet page")
        try:
            ts = self.session.service("ALTabletService")
            ts.resetTablet()
            ts._clearWebviewCache(1)
            ts.cleanWebview()
            ts.hideWebview()
        except Exception, e:
            self.logger.info("Error while hiding tablet: {}".format(e))
                
                
                
    ############################################################

    def cleanup(self):
        # called when your module is stopped
        self.logger.info("Cleaning...")
        # don't forget to finish cleanly 
        self.hide_tablet()
        self.remove_memory_subscribers()

        self.logger.info("End!")

if __name__ == "__main__":
    # with this you can run the script for tests on remote robots
    # run : python my_super_service.py --qi-url 123.123.123.123
    app = qi.Application(sys.argv)
    app.start()
    service_instance = FGAppLauncher(app)
    service_id = app.session.registerService(service_instance.service_name, service_instance)
    service_instance.start_service()
    app.run()
    service_instance.cleanup()
    app.session.unregisterService(service_id)
